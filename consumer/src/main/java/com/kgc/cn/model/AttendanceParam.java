package com.kgc.cn.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


@Data
@ApiModel(value = "出勤model")
public class AttendanceParam implements Serializable {
    private static final long serialVersionUID = 3889447556274515645L;
    @ApiModelProperty(value = "工号")
    private String empId;
    @ApiModelProperty(value = "姓名")
    private String empName;
    @ApiModelProperty(value = "部门name")
    private String departName;
}