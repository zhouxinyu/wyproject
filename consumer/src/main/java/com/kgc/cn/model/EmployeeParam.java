package com.kgc.cn.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


@Data
@ApiModel(value = "员工model")
public class EmployeeParam implements Serializable {
    private static final long serialVersionUID = -2205105278652819098L;
    @ApiModelProperty(value = "工号")
    private String empId;
    @ApiModelProperty(value = "姓名")
    private String empName;
    @ApiModelProperty(value = "性别", example = "1")
    private Integer empSex;
    @ApiModelProperty(value = "年龄", example = "1")
    private Integer empAge;
    @ApiModelProperty(value = "邮箱")
    private String empEmail;
    @ApiModelProperty(value = "手机号")
    private String empPhone;
    @ApiModelProperty(value = "密码")
    private String empPassword;
    @ApiModelProperty(value = "地址")
    private String empAddress;
    @ApiModelProperty(value = "身份证")
    private String empIdentityCard;
    @ApiModelProperty(value = "入职时间")
    private String empTimeStart;
    @ApiModelProperty(value = "工资", example = "1")
    private Double empSalary;
    @ApiModelProperty(value = "权限id", example = "1")
    private Integer roleId;
    @ApiModelProperty(value = "部门id", example = "1")
    private Integer departId;
}