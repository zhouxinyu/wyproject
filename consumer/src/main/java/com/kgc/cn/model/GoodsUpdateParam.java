package com.kgc.cn.model;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiOperation(value = "商品修改model")
public class GoodsUpdateParam implements Serializable {
    private static final long serialVersionUID = -402503897028934547L;

    @ApiModelProperty(value = "商品ID")
    private String goodId;
    @ApiModelProperty(value = "商品名")
    private String goodName;
    @ApiModelProperty(value = "单价", example = "1")
    private Integer goodPrice;
    @ApiModelProperty(value = "类型id", example = "1")
    private Integer typeId;
    @ApiModelProperty(value = "图片地址")
    private String pictureSource;
    @ApiModelProperty(value = "内容")
    private String goodContent;
    @ApiModelProperty(value = "是否下架：0：下架，1：未下架", example = "1")
    private Integer isDelete;
}
