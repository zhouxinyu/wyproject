package com.kgc.cn.model;

import com.kgc.cn.model.dto.Department;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Created by Ding on 2019/12/14.
 */
@Data
@ApiModel(value = "缺勤model")
public class AbsenteeismParam {
    @ApiModelProperty(value = "工号")
    private String empId;
    @ApiModelProperty(value = "姓名")
    private String empName;
    @ApiModelProperty(value = "部门")
    private Department department;
}
