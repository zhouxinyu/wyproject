package com.kgc.cn.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

/**
 * @auther zhouxinyu
 * @data 2019/12/15
 */
@ApiModel(value = "商品庫存model")
@Data
@Builder
public class GoodsNumParam {
    @ApiModelProperty(value = "商品名")
    private String goodName;
    @ApiModelProperty(value = "库存", example = "1")
    private Integer goodNum;
    @ApiModelProperty(value = "类型名")
    private String typeName;
}
