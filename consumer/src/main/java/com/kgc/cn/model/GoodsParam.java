package com.kgc.cn.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "商品model")
public class GoodsParam implements Serializable {
    private static final long serialVersionUID = 1285309073301937818L;
    @ApiModelProperty(value = "商品ID")
    private String goodId;
    @ApiModelProperty(value = "商品名")
    private String goodName;
    @ApiModelProperty(value = "单价", example = "1")
    private Integer goodPrice;
    @ApiModelProperty(value = "库存", example = "1")
    private Integer goodNum;
    @ApiModelProperty(value = "类型id", example = "1")
    private Integer typeId;
    @ApiModelProperty(value = "图片地址")
    private String pictureSource;
    @ApiModelProperty(value = "内容")
    private String goodContent;
    @ApiModelProperty(value = "是否下架：0：下架，1：未下架", example = "1")
    private Integer isDelete;
}