package com.kgc.cn.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Set;

@Data
@ApiModel(value = "促销model")
public class PromotionParam implements Serializable {
    private static final long serialVersionUID = 3340361423503069315L;
    @ApiModelProperty(value = "开始时间")
    private String timeStart;
    @ApiModelProperty(value = "结束时间")
    private String timeEnd;
    @ApiModelProperty(value = "折扣力度：10为一折", example = "1")
    private Integer discount;
    @ApiModelProperty(value = "商品id集合")
    private Set<String> goodIdSet;
}