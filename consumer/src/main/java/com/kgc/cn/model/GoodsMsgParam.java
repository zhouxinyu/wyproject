package com.kgc.cn.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Created by boot on 2019/12/16
 */
@Data
@ApiModel(value = "商品信息显示model")
public class GoodsMsgParam {
    @ApiModelProperty(value = "商品ID")
    private String goodId;
    @ApiModelProperty(value = "商品名")
    private String goodName;
    @ApiModelProperty(value = "单价", example = "1")
    private Integer goodPrice;
    @ApiModelProperty(value = "库存", example = "1")
    private Integer goodNum;
    @ApiModelProperty(value = "图片地址")
    private String pictureSource;
    @ApiModelProperty(value = "内容")
    private String goodContent;
    @ApiModelProperty(value = "商品是否已经下架信息")
    private String isDeleteMsg;
    @ApiModelProperty(value = "商品类型")
    private String typeName;
}
