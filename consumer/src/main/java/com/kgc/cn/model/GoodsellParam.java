package com.kgc.cn.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 出售记录表
 */
@ApiModel(value = "售出model")
@Data
public class GoodsellParam implements Serializable {
    private static final long serialVersionUID = 7308746867205264845L;
    @ApiModelProperty(value = "售出商品id")
    private String goodId;
    @ApiModelProperty(value = "售出的总数量", example = "1")
    private Integer sellNum;
    @ApiModelProperty(value = "售出的总价", example = "1")
    private Double sellMoney;
}