package com.kgc.cn.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Created by Ding on 2019/12/13.
 */
@Data
@ApiModel(value = "出售总量moedl")
public class SellParam {

    @ApiModelProperty(value = "总数量", example = "1")
    private Integer numSum;
    @ApiModelProperty(value = "总价格", example = "1")
    private Double moneySum;
}
