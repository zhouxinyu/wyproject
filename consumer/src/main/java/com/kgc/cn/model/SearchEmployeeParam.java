package com.kgc.cn.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Created by boot on 2019/12/13
 */
@Data
@ApiModel(value = "搜索条件model")
public class SearchEmployeeParam {
    @ApiModelProperty(value = "搜索条件员工id")
    private String id;
    @ApiModelProperty(value = "搜索条件员工name")
    private String name;
    @ApiModelProperty(value = "搜索条件起始日期")
    private String timeStart;
    @ApiModelProperty(value = "搜索条件终止日期")
    private String timeEnd;

}
