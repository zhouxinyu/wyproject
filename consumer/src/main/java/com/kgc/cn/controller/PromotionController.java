package com.kgc.cn.controller;

import com.kgc.cn.config.aop.CurrentUser;
import com.kgc.cn.config.aop.LoginRequired;
import com.kgc.cn.consumerService.EmployeeConsumerService;
import com.kgc.cn.consumerService.PromotionConsumerService;
import com.kgc.cn.enums.PromotionEnum;
import com.kgc.cn.model.EmployeeParam;
import com.kgc.cn.model.PageBeanParam;
import com.kgc.cn.model.PromotionDataParam;
import com.kgc.cn.model.PromotionParam;
import com.kgc.cn.returnResult.ReturnResult;
import com.kgc.cn.returnResult.ReturnResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 促销controller层
 * Created by Ding on 2019/12/12.
 */
@RestController
@Api(tags = "促销活动")
@RequestMapping("/promotion")
public class PromotionController {
    @Autowired
    private PromotionConsumerService promotionService;
    @Autowired
    private EmployeeConsumerService employeeService;

    /**
     *LoginRequired  roleId = 1 管理员权限
     * @param employeeParam
     * @param promotionParam
     * @return
     * @throws Exception
     */
    @LoginRequired(roleId = 1)
    @ApiOperation(value = "修改促销活动")
    @PostMapping(value = "/updatePromotion")
    public ReturnResult updatePromotion(@CurrentUser EmployeeParam employeeParam, @Valid PromotionParam promotionParam) throws Exception {

            //1.前端的值传递
            String isOk = promotionService.updatePromotion(promotionParam);
            if (isOk.contains("不存在的商品")) {
                return ReturnResultUtils.returnFail(PromotionEnum.UPDATE_ERROR,isOk);
            } else if ("1".equals(isOk)){
                return ReturnResultUtils.returnFail(PromotionEnum.ENDTIME_ERROR);
            } else if ("0".equals(isOk)){
                return ReturnResultUtils.returnFail(PromotionEnum.STARTTIME_ERROR);
            } else {
                return ReturnResultUtils.returnSuccess(PromotionEnum.VALUESUCCESS);
            }
    }

    /**
     * 权限：所有员工
     * 根据商品id查询，如果有促销，但日期结束了，则调用删除方法，删除该条促销活动，返回该商品没有促销活动。
     */
    @LoginRequired
    @ApiOperation(value = "精确查询促销活动")
    @PostMapping(value = "/queryPromotion")
    public ReturnResult queryPromotion(@ApiParam("查询的商品ID") @RequestParam String goodId) throws Exception {
        PromotionDataParam flag = promotionService.queryPromotion(goodId);
        if (flag == null) {
            return ReturnResultUtils.returnFail(PromotionEnum.PROMOTION);
        } else {
            return ReturnResultUtils.returnSuccess(flag);
        }
    }

    /**
     * 所有促销活动分页查询
     *
     * @param current
     * @param size
     * @return
     */
    @LoginRequired
    @ApiOperation(value = "查询所有的促销活动")
    @PostMapping(value = "/queryAllPromotion")
    public ReturnResult queryAllPromotion(@ApiParam("当前页") @RequestParam(defaultValue = "1") String current,
                                          @ApiParam("每页大小") @RequestParam(defaultValue = "5") String size) {
        //转成int型
        int sizeInt = Integer.parseInt(size);
        int currentInt = Integer.parseInt(current);
        PageBeanParam<PromotionDataParam> pageBeanParam = promotionService.promotionPage(currentInt, sizeInt);
        //查询所有未结束的活动
        if (null == pageBeanParam.getData()) {
            return ReturnResultUtils.returnFail(PromotionEnum.PROMOTION_EMPTY);
        }
        return ReturnResultUtils.returnSuccess(pageBeanParam);
    }

    /**
     * 管理员权限增加促销
     *
     * @param promotionParam
     * @return
     */
    @ApiOperation(value = "增加促销活动")
    @PostMapping(value = "/addPromotion")
    @LoginRequired(roleId = 1)
    public ReturnResult addPromotion(@Valid PromotionParam promotionParam) {
        // 判断结果
        String flag = promotionService.addPromotion(promotionParam);
        // 日期格式错误
        if (flag.equals("2")) return ReturnResultUtils.returnFail(PromotionEnum.DATE_FORMATE_ERROR);
        // 未输入商品ID
        if (flag.equals("4")) return ReturnResultUtils.returnFail(PromotionEnum.NULL_VALUE_ERROR);
        // 结束时间早于开始时间
        if (flag.equals("6")) return ReturnResultUtils.returnFail(PromotionEnum.DATE_ORDER_ERROR);
        // 开始时间早于当前时间
        if (flag.equals("7")) return ReturnResultUtils.returnFail(PromotionEnum.DATE_START_ERROR);

        return ReturnResultUtils.returnSuccess(flag);
    }
}
