package com.kgc.cn.consumerService;


import com.kgc.cn.model.PageBeanParam;
import com.kgc.cn.model.PromotionDataParam;
import com.kgc.cn.model.PromotionParam;
import com.kgc.cn.model.dto.Promotion;

/**
 * 2.
 * Created by Ding on 2019/12/12.
 */
public interface PromotionConsumerService {
    // 更新促销活动信息
    String updatePromotion(PromotionParam promotionParam) throws Exception;

    // 查询活动,删除过期活动
    PromotionDataParam queryPromotion(String goodId) throws Exception;

    //所有活动查询分页
    PageBeanParam<PromotionDataParam> promotionPage(int current, int size);

    // 增加折扣
    String addPromotion(PromotionParam promotionParam);
}
