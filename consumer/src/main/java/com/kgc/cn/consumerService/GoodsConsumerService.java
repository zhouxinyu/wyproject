package com.kgc.cn.consumerService;

import com.kgc.cn.model.GoodsMsgParam;
import com.kgc.cn.model.GoodsUpdateParam;
import com.kgc.cn.model.PageBeanParam;
import com.kgc.cn.model.SellParam;

import java.io.IOException;
import java.util.Set;

/**
 * Created by Ding on 2019/12/12.
 */
public interface GoodsConsumerService {
    //查询售出的商品总数量和总价格
    SellParam querySell();

    //通过excel表格批量添加商品
    String addGoods(String filePath) throws IOException;

    // 查詢商品的總量
    PageBeanParam queryGoodsNum(int current, int size);

    // 通过商品ID和商品名模糊查询商品，如果条件为空，则返回所有商品。
    PageBeanParam<GoodsMsgParam> queryGoodsInfo(int current, int size, String goodsId, String goodsName);

    //更新商品信息
    String updateGoods(GoodsUpdateParam goodsUpdateParam);

    // 根据商品id删除
    String deleteGoodsByGoodIds(Set<String> goodIdSet);
}
