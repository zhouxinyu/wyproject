package com.kgc.cn.config.aop.impl;

import com.alibaba.fastjson.JSONObject;
import com.kgc.cn.config.aop.LoginRequired;
import com.kgc.cn.model.EmployeeParam;
import com.kgc.cn.utils.redis.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

public class LoginAuthentication implements HandlerInterceptor {

    @Autowired
    private RedisUtils redisUtils;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        //判断是不是controller层的方法
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }

        //判断方法有没有@LoginRequired注解
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();
        LoginRequired loginRequired = method.getAnnotation(LoginRequired.class);

        // 如果有被此注解标识
        if (loginRequired != null) {
            String token = request.getHeader("token");
            if (!StringUtils.isEmpty(token)) {
                if (redisUtils.hasKey(token)) {
                    String userJsonStr = redisUtils.get(token).toString();
                    //判断用户对象字符串是否为空
                    if (null != userJsonStr) {
                        request.setAttribute("userJsonStr", userJsonStr);
                        EmployeeParam employeeParam = JSONObject.parseObject(userJsonStr, EmployeeParam.class);
                        //判断登录用户的权限是否满足
                        if (employeeParam.getRoleId() > loginRequired.roleId()) {
                            throw new RuntimeException("权限不足");
                        }
                    }
                }else {
                    throw new RuntimeException("数据传入错误!");
                }
            } else {
                throw new RuntimeException("当前无用户登录!");
            }
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
