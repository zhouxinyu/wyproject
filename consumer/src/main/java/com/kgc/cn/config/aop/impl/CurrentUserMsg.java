package com.kgc.cn.config.aop.impl;

import com.alibaba.fastjson.JSONObject;
import com.kgc.cn.config.aop.CurrentUser;
import com.kgc.cn.model.EmployeeParam;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import java.util.Objects;


public class CurrentUserMsg implements HandlerMethodArgumentResolver {

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        //判断所需参数是不是user并且有没有@CurrentUser注解
        return parameter.getParameterType().isAssignableFrom(EmployeeParam.class) &&
                parameter.hasParameterAnnotation(CurrentUser.class);
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        String userJsonStr = webRequest.getAttribute("userJsonStr", RequestAttributes.SCOPE_REQUEST).toString();
        EmployeeParam employeeParam = JSONObject.parseObject(userJsonStr, EmployeeParam.class);
        if (!Objects.isNull(employeeParam)) {
            return employeeParam;
        }
        return null;
    }
}
