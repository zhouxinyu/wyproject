package com.kgc.cn.AsayncClass;

import com.kgc.cn.consumerService.EmployeeConsumerService;
import com.kgc.cn.consumerService.GoodsConsumerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import java.util.concurrent.Future;

/**
 * @auther zhouxinyu
 * @data 2019/12/19
 */
@Component
public class AsyncTask {
    @Autowired
    private EmployeeConsumerService employeeConsumerService;
    @Autowired
    private GoodsConsumerService goodsConsumerService;

    /**
     * 异步调用添加员工
     * @param filePath
     * @return
     * @throws Exception
     */
    @Async
    public Future<String> addEmployee(String filePath) throws Exception {
        return new AsyncResult<String>(employeeConsumerService.addEmployee(filePath));
    }

    /**
     * 异步调用添加商品
     * @param filePath
     * @return
     * @throws Exception
     */
    @Async
    public Future<String> addGoods(String filePath) throws Exception {
        return new AsyncResult<String>(goodsConsumerService.addGoods(filePath));
    }

}
