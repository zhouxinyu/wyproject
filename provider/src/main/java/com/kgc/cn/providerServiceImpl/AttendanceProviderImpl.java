package com.kgc.cn.providerServiceImpl;

import com.alibaba.dubbo.config.annotation.Service;
import com.kgc.cn.commonService.AttendanceConmonService;
import com.kgc.cn.mapper.AttendanceMapper;
import com.kgc.cn.model.dto.Attendance;
import com.kgc.cn.utils.Date.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.util.Date;

/**
 * Created by Ding on 2019/12/13.
 */
@Service
public class AttendanceProviderImpl implements AttendanceConmonService {
    @Autowired
    private AttendanceMapper attendanceMapper;

    /**
     * 查询员工的出勤记录
     *
     * @param empId
     * @return
     */
    @Override
    public Attendance queryAttendance(String empId) {
        return attendanceMapper.selectByPrimaryKey(empId);
    }

    /**
     * 增加出勤记录
     *
     * @param empId
     * @return
     * @throws ParseException
     */
    @Transactional
    @Override
    public int insertAttendance(String empId) throws ParseException {
        Attendance attendance = Attendance.builder().empId(empId)
                .workTime(DateUtils.accurateDay(new Date())).build();
        return attendanceMapper.insert(attendance);
    }

    /**
     * 更新出勤日期
     *
     * @param empId
     * @return
     * @throws ParseException
     */
    @Override
    public int updateWorkTime(String empId) throws ParseException {
        Attendance attendance = Attendance.builder().empId(empId)
                .workTime(DateUtils.accurateDay(new Date())).build();
        return attendanceMapper.updateByPrimaryKey(attendance);
    }
}
