package com.kgc.cn.providerServiceImpl;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.metadata.Sheet;
import com.google.common.collect.Lists;
import com.kgc.cn.commonService.GoodsConmonService;
import com.kgc.cn.mapper.GoodsMapper;
import com.kgc.cn.mapper.GoodsellMapper;
import com.kgc.cn.model.dto.Goods;
import com.kgc.cn.model.dto.GoodsExample;
import com.kgc.cn.model.dto.Goodsell;
import com.kgc.cn.utils.GoodUtil;
import com.kgc.cn.utils.excel.GoodsExcelModel;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by Ding on 2019/12/12.
 */

@Service
public class GoodsProviderImp implements GoodsConmonService {
    @Autowired
    private GoodsellMapper goodsellMapper;

    /**
     * 查询售出商品总数量和总价格
     *
     * @return
     */
    @Override
    public Goodsell querySell() {
        return goodsellMapper.querySell();
    }

    @Autowired
    private GoodUtil goodUtil;
    @Autowired
    private GoodsMapper goodsMapper;


    /**
     * 读取excel表格数据添加商品到数据库
     *
     * @param filePath
     * @return
     * @throws IOException
     */
    @Override
    public String addGoods(String filePath) throws IOException {
        // 读取excel数据，放到一个list里面
        Sheet sheet = new Sheet(1, 1, GoodsExcelModel.class);
        InputStream inputStream = new FileInputStream(new File(filePath));
        List<Object> list = EasyExcelFactory.read(inputStream, sheet);
        // 转换类型
        List<GoodsExcelModel> listVo = Lists.newArrayList();
        for (Object o : list) {
            listVo.add((GoodsExcelModel) o);
        }
        // 遍历转换类型后的list，根据取出的商品的类型生成商品id，将前台给的商品vo复制到商品dto，
        String str = "";
        for (GoodsExcelModel ExcelModel : listVo) {
            Goods goods = new Goods();
            BeanUtils.copyProperties(ExcelModel, goods);
            Goods goodsold = goodNameisExist(goods.getGoodName());
            // 判断苦衷是否有这个商品名
            if (goodsold == null) {
                //不存在商品名
                //同时赋值给商品dto的商品id（调用方法自动生成）
                String generateGoodId = goodUtil.generateGoodId(ExcelModel.getTypeId());
                goods.setGoodId(generateGoodId);
                // 给商品isdelete赋值默认值（1）
                goods.setIsDelete(1);
                // 存到数据库
                goodsMapper.insert(goods);
            } else if (goodsold.getIsDelete() == 1) {
                int sum = goodsold.getGoodNum() + goods.getGoodNum();
                goodsold.setGoodNum(sum);
                goodsMapper.updateByPrimaryKeySelective(goodsold);
            } else {
                str += goods.getGoodName() + ",";
            }
        }
        if (str.length() == 0) {
            return "0";
        } else {
            return str;
        }
    }

    /**
     * 查询商品名是否存在
     *
     * @return
     */
    @Override
    public Goods goodNameisExist(String name) {
        GoodsExample goodsExample = new GoodsExample();
        GoodsExample.Criteria criteria = goodsExample.createCriteria();
        criteria.andGoodNameEqualTo(name);
        List<Goods> list = goodsMapper.selectByExample(goodsExample);
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        return list.get(0);
    }

    /**
     * 查找库里所有商品的库存
     * 用于分页
     *
     * @return
     */
    @Override
    public List<Goods> queryGoodsNum(int current, int size) {
        // 查找库里所有信息
        int start = (current - 1) * size;
        return goodsMapper.selectAllGoods(start, size);
    }

    /**
     * 查询商品总数量
     *
     * @return
     */
    @Override
    public int queryCount() {
        return goodsMapper.queryCount();
    }


    /**
     * 根据商品id和商品名模糊查询商品
     *
     * @param goodId
     * @param goodName
     * @return
     */
    @Override
    public List<Goods> queryGoodsInfo(int current, int size, String goodId, String goodName) {
        int start = (current - 1) * size;
        return goodsMapper.queryGoodsInfo(start, size, goodId, goodName);
    }

    /**
     * 更新商品信息，除了id，库存以外均可以修改
     *
     * @param goods
     * @return
     */
    @Override
    public boolean updateGoods(Goods goods) {
        if (goodsMapper.selectByPrimaryKey(goods.getGoodId()) == null) {
            return false;
        } else {
            int flag = goodsMapper.updateGoods(goods);
            if (flag >= 1) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * 管理员权限根据商品ID下架商品
     *
     * @param goodId
     * @return
     */
    @Override
    public int deleteGoodsByGoodIds(String goodId) {
        return goodsMapper.deleteGoodsByGoodIds(goodId);
    }

    /**
     * 查询商品总数
     *
     * @return
     */
    @Override
    public int queryGoodCount(int current, int size, String goodId, String goodName) {
        int start = (current - 1) * size;
        return goodsMapper.queryGoodCount(start, size, goodId, goodName);
    }

    /**
     * 判断对应ID有无商品
     *
     * @param goodId
     * @return
     */
    @Override
    public boolean hasGood(String goodId) {
        Goods goods = goodsMapper.selectByPrimaryKey(goodId);

        if (null == goods) return false;

        return true;
    }


}
