package com.kgc.cn.providerServiceImpl;

import com.alibaba.dubbo.config.annotation.Service;
import com.kgc.cn.commonService.PromotionConmonService;
import com.kgc.cn.mapper.GoodsMapper;
import com.kgc.cn.mapper.PromotionMapper;
import com.kgc.cn.model.dto.Goods;
import com.kgc.cn.model.dto.Promotion;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Ding on 2019/12/12.
 */
@Service
public class promotionProviderImpl implements PromotionConmonService {
    @Autowired
    private PromotionMapper promotionMapper;

    @Autowired
    private GoodsMapper goodsMapper;

    /**
     * 修改促销活动
     * 促销活动未开始，修改折扣和时间；在活动期间依然可以修改促销活动。
     *
     * @param promotion
     * @return
     * @throws Exception
     */
    @Transactional
    @Override
    public String updatePromotion(Promotion promotion) throws Exception {
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        Date timeStart = sf.parse(promotion.getTimeStart());
        Date timeEnd = sf.parse(promotion.getTimeEnd());
        Date timeNow = new Date();
        String str = "";
        String strNotExist = "不存在的商品ID：";
        int sum = 0;
        //遍历goodIdSet集合，存在的goodid就添加存到数据库，不存在的goodid就标记
        for (String goodid : promotion.getGoodIdSet()) {
            //返回的信息可以知道产品是否存在
            Promotion promotionIsExist = promotionMapper.selectByPrimaryKey(goodid);
            if (null == promotionIsExist) {
                //记录不存在的goodid
                strNotExist += goodid + ",";
            } else {
                if ( timeStart.before(timeNow)) {
                    //给其赋值，提示错误信息，返回前端用来提醒
                    str = "0";
                } else {
                    if (timeStart.after(timeEnd)) {
                        str = "1";
                    } else {
                        Promotion promotionnew = new  Promotion();
                        BeanUtils.copyProperties(promotion,promotionnew);
                        //这里goodid为空，所以要存入存在的goodid
                        promotionnew.setGoodId(goodid);
                        //sum计录有效的商品数
                        sum += promotionMapper.updatePromotionById(promotionnew);
                    }
                }
            }
        }
        //用sum来和刚开始传入的goodidset数量比较，都相同，返回2表示修改成功；数量不同，返回不存在的商品id
        if(sum == promotion.getGoodIdSet().size()){
            return "2";
        }else if(StringUtils.isEmpty(str)){
            return strNotExist;
        }else {
            //返回错误信息
            return str;
        }
    }

    /**
     * 通过商品id删除id对应的促销活动
     *
     * @param goodId
     * @return
     */
    @Override
    public int deletePromotion(String goodId) {
        return promotionMapper.deletePromotion(goodId);
    }

    /**
     * 通过商品id查询id对应的促销活动，删除过期活动
     *
     * @param goodId
     * @return
     */
    @Override
    public Promotion queryPromotion(String goodId) throws Exception {
        if (promotionMapper.selectByPrimaryKey(goodId) != null) {
            Promotion promotion = promotionMapper.queryPromotion(goodId);
            return promotion;
        } else {
            return null;
        }

    }

    /**
     * 查询所有促销活动
     * @param current
     * @param size
     * @return
     */
    @Override
    public List<Promotion> promotionPage(int current, int size) {
        int start = (current - 1) * size;
        return promotionMapper.promotionPage(start, size);
    }

    /**
     * 查询促销活动总数量
     * 用于分页
     *
     * @return
     */
    @Override
    public int queryCount() {
        return promotionMapper.queryCount();
    }

    /**
     * 更新促销活动
     *
     * @param promotion
     * @return
     */
    @Override
    public int updateByPrimaryKey(Promotion promotion) {
        return promotionMapper.updateByPrimaryKey(promotion);
    }

    /**
     * 增加促销活动
     *
     * @param promotion
     * @return
     */
    @Transactional
    @Override
    public int addPromotion(Promotion promotion) {
        return promotionMapper.insert(promotion);
    }

    /**
     * 判断数据库中是否已经存在某个商品的折扣
     *
     * @param goodId
     * @return
     */
    @Override
    public boolean hasPromorion(String goodId) {
        if (null != promotionMapper.selectByPrimaryKey(goodId)) {
            return true;
        }
        return false;
    }

    /**
     * 判断要添加的商品有没有下架
     *
     * @param goodId
     * @return
     */
    @Override
    public boolean isDelete(String goodId) {
        Goods goods = goodsMapper.selectByPrimaryKey(goodId);
        if (goods.getIsDelete() != 0) {
            return false;
        }
        return true;
    }

}
