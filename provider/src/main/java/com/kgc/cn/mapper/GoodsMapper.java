package com.kgc.cn.mapper;

import com.kgc.cn.model.dto.Goods;
import com.kgc.cn.model.dto.GoodsExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GoodsMapper {
    long countByExample(GoodsExample example);

    int deleteByExample(GoodsExample example);

    int deleteByPrimaryKey(String goodId);

    int insert(Goods record);

    int insertSelective(Goods record);

    List<Goods> selectByExample(GoodsExample example);

    Goods selectByPrimaryKey(String goodId);

    int updateByExampleSelective(@Param("record") Goods record, @Param("example") GoodsExample example);

    int updateByExample(@Param("record") Goods record, @Param("example") GoodsExample example);

    int updateByPrimaryKeySelective(Goods record);

    int updateByPrimaryKey(Goods record);

    // 根据id和商品名模糊查询商品
    List<Goods> queryGoodsInfo(@Param("start") int start, @Param("size") int size, @Param("goodId") String goodId, @Param("goodName") String goodName);

    //按商品类型返回所有商品的庫存
    List<Goods> selectAllGoods(@Param("start") int start, @Param("size") int size);

    //修改商品信息
    int updateGoods(Goods goods);

    //查询出勤表数据总数量
    int queryCount();

    // 根据商品id下架商品（改状态）
    int deleteGoodsByGoodIds(String goodId);

    // 查询查询出的商品总数
    int queryGoodCount(@Param("start") int start, @Param("size") int size, @Param("goodId") String goodId, @Param("goodName") String goodName);
}