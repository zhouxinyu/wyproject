package com.kgc.cn.mapper;

import com.kgc.cn.model.dto.Promotion;
import com.kgc.cn.model.dto.PromotionExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PromotionMapper {
    long countByExample(PromotionExample example);

    int deleteByExample(PromotionExample example);

    int deleteByPrimaryKey(String goodId);

    int insert(Promotion record);

    int insertSelective(Promotion record);

    List<Promotion> selectByExample(PromotionExample example);

    Promotion selectByPrimaryKey(String goodId);

    int updateByExampleSelective(@Param("record") Promotion record, @Param("example") PromotionExample example);

    int updateByExample(@Param("record") Promotion record, @Param("example") PromotionExample example);

    int updateByPrimaryKeySelective(Promotion record);

    int updateByPrimaryKey(Promotion record);

    //更新促销
    int updatePromotion(Promotion promotion);

    //删除促销
    int deletePromotion(@Param("goodId") String goodId);

    //精确查询促销
    Promotion queryPromotion(String goodId);

    //查询所有促销活动
    List<Promotion> promotionPage(@Param("start") int start, @Param("size") int size);

    //查询促销活动总数量
    int queryCount();

    //修改促销活动
    int updatePromotionById(@Param("promotionnew") Promotion promotionnew);


}