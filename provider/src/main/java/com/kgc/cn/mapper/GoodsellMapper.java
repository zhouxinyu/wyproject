package com.kgc.cn.mapper;

import com.kgc.cn.model.dto.Goodsell;
import com.kgc.cn.model.dto.GoodsellExample;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface GoodsellMapper {
    long countByExample(GoodsellExample example);

    int deleteByExample(GoodsellExample example);

    int deleteByPrimaryKey(String goodId);

    int insert(Goodsell record);

    int insertSelective(Goodsell record);

    List<Goodsell> selectByExample(GoodsellExample example);

    Goodsell selectByPrimaryKey(String goodId);

    int updateByExampleSelective(@Param("record") Goodsell record, @Param("example") GoodsellExample example);

    int updateByExample(@Param("record") Goodsell record, @Param("example") GoodsellExample example);

    int updateByPrimaryKeySelective(Goodsell record);

    int updateByPrimaryKey(Goodsell record);

    //查询售出信息
    Goodsell querySell();
}