package com.kgc.cn.utils;

import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * @auther zhouxinyu
 * @data 2019/12/12
 */
@Component
public class GoodUtil {
    /**
     * 根据商品类型和自动生成器来生成商品id
     *
     * @return
     */
    public String generateGoodId(int typeId) {
        return typeId + UUID.randomUUID().toString().replaceAll("-", "").substring(0, 17);
    }

}
