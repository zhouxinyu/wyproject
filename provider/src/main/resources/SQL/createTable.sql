/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50712
Source Host           : localhost:3306
Source Database       : wangyishop

Target Server Type    : MYSQL
Target Server Version : 50712
File Encoding         : 65001

Date: 2019-12-18 11:26:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for attendance
-- ----------------------------
DROP TABLE IF EXISTS `attendance`;
CREATE TABLE `attendance` (
  `empId` varchar(50) NOT NULL COMMENT '员工id',
  `workTime` varchar(15) NOT NULL COMMENT '出勤日期(xxxx-xx-xx)',
  PRIMARY KEY (`empId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of attendance
-- ----------------------------
INSERT INTO `attendance` VALUES ('1', '2019-12-18');
INSERT INTO `attendance` VALUES ('2', '2019-12-17');
INSERT INTO `attendance` VALUES ('3', '2019-12-17');

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `departId` int(2) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `department` varchar(10) NOT NULL COMMENT '部门名',
  PRIMARY KEY (`departId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of department
-- ----------------------------
INSERT INTO `department` VALUES ('1', '行政部');
INSERT INTO `department` VALUES ('2', '人事部');
INSERT INTO `department` VALUES ('3', '市场部');
INSERT INTO `department` VALUES ('4', '财务部');
INSERT INTO `department` VALUES ('5', 'IT部');
INSERT INTO `department` VALUES ('6', '采购部');
INSERT INTO `department` VALUES ('7', '仓储物流');

-- ----------------------------
-- Table structure for employee
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee` (
  `empId` varchar(50) NOT NULL COMMENT '员工工号',
  `empName` varchar(10) NOT NULL COMMENT '姓名',
  `empSex` int(2) NOT NULL COMMENT '性别1:男2:女',
  `empAge` int(4) NOT NULL COMMENT '年龄',
  `empEmail` varchar(30) NOT NULL COMMENT '邮箱',
  `empPhone` varchar(15) NOT NULL COMMENT '手机号',
  `empPassword` varchar(50) NOT NULL COMMENT '密码',
  `empAddress` varchar(50) DEFAULT NULL COMMENT '住址',
  `empIdentityCard` varchar(20) NOT NULL COMMENT '身份证',
  `empTimeStart` varchar(15) NOT NULL COMMENT '入职时间(xxxx-xx-xx)',
  `empSalary` double(8,2) NOT NULL COMMENT '基本工资',
  `isDelete` int(2) NOT NULL COMMENT '是否删除1:已经删除0:未删除',
  `roleId` int(2) NOT NULL COMMENT '权限id',
  `departId` int(2) NOT NULL COMMENT '部门id',
  PRIMARY KEY (`empId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of employee
-- ----------------------------
INSERT INTO `employee` VALUES ('1', '丁鹏', '2', '2', '2', '1', 'QAHlVoUc49w=', '2', '1', '1998-05-06', '1000.00', '0', '1', '1');
INSERT INTO `employee` VALUES ('2', '周新宇', '2', '2', '2', '2', 'QAHlVoUc49w=', '2', '1', '1998-05-06', '2000.00', '0', '2', '2');
INSERT INTO `employee` VALUES ('3', '张靖', '2', '3', '3', '3', 'QAHlVoUc49w=', '3', '3', '1998-05-06', '3000.00', '0', '3', '3');

-- ----------------------------
-- Table structure for goods
-- ----------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods` (
  `goodId` varchar(50) NOT NULL COMMENT '商品id',
  `goodName` varchar(30) NOT NULL COMMENT '商品名字',
  `goodPrice` int(10) NOT NULL COMMENT '价格(单位为分，算出折扣后的值一定要保留两位小数)',
  `goodNum` int(2) NOT NULL COMMENT '库存',
  `typeId` int(2) NOT NULL COMMENT '类型id',
  `pictureSource` varchar(80) DEFAULT NULL COMMENT '图片地址',
  `goodContent` varchar(500) DEFAULT NULL COMMENT '商品简介',
  `isDelete` int(2) NOT NULL COMMENT '是否下架 0是下架，1是未下架',
  PRIMARY KEY (`goodId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of goods
-- ----------------------------

-- ----------------------------
-- Table structure for goodsell
-- ----------------------------
DROP TABLE IF EXISTS `goodsell`;
CREATE TABLE `goodsell` (
  `goodId` varchar(50) NOT NULL COMMENT '商品id',
  `sellNum` int(10) NOT NULL COMMENT '售出数量',
  `sellMoney` double(50,2) NOT NULL COMMENT '售出总价',
  PRIMARY KEY (`goodId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of goodsell
-- ----------------------------
INSERT INTO `goodsell` VALUES ('1', '1', '50.99');
INSERT INTO `goodsell` VALUES ('2', '3', '60.56');
INSERT INTO `goodsell` VALUES ('3', '10', '120.00');

-- ----------------------------
-- Table structure for goodtype
-- ----------------------------
DROP TABLE IF EXISTS `goodtype`;
CREATE TABLE `goodtype` (
  `typeId` int(2) NOT NULL AUTO_INCREMENT COMMENT '类型id',
  `typeName` varchar(10) NOT NULL COMMENT '类型名',
  PRIMARY KEY (`typeId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of goodtype
-- ----------------------------
INSERT INTO `goodtype` VALUES ('1', '居家生活');
INSERT INTO `goodtype` VALUES ('2', '服饰鞋包');
INSERT INTO `goodtype` VALUES ('3', '美食酒水');
INSERT INTO `goodtype` VALUES ('4', '个护清洁');
INSERT INTO `goodtype` VALUES ('5', '母婴亲子');
INSERT INTO `goodtype` VALUES ('6', '运动旅行');
INSERT INTO `goodtype` VALUES ('7', '数码家电');

-- ----------------------------
-- Table structure for promotion
-- ----------------------------
DROP TABLE IF EXISTS `promotion`;
CREATE TABLE `promotion` (
  `goodId` varchar(50) NOT NULL COMMENT '商品id',
  `timeStart` varchar(15) NOT NULL COMMENT '开始时间',
  `timeEnd` varchar(15) NOT NULL COMMENT '到期时间',
  `discount` int(4) NOT NULL COMMENT '折扣力度;  示例一折对应值为10,六五折对应值为65，算价格时将值转成double除以100',
  PRIMARY KEY (`goodId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of promotion
-- ----------------------------

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `roleId` int(2) NOT NULL AUTO_INCREMENT COMMENT '权限id',
  `roleName` varchar(10) NOT NULL COMMENT '权限名',
  PRIMARY KEY (`roleId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', '管理员');
INSERT INTO `role` VALUES ('2', '录入员');
INSERT INTO `role` VALUES ('3', '普通员工');
