package com.kgc.cn.enums;

/**
 * @auther zhouxinyu
 * @data 2019/12/13
 */
public enum GoodsEnum {
    GOODS_ADDSUCESS("商品数据添加成功！"),
    GOODS_ADDISDELETE(301, "添加失败，商品已经被删除！添加失敗的商品是："),
    GOODS_ADDFAILTURE(302, "书籍添加失败"),
    ROLE_ERROR(303,"权限不足，无法修改"),
    GOODS_UPDATEFAIL(304,"数据修改失败"),
    GOODS_NULL(306,"最少输入一条修改数据"),
    GOODS_EMPTY(307,"商品id不存在，修改失败"),
    GOODS_UPDATESUCCESS(305,"数据修改成功"),
    NULL_VALUE_ERROR(200, "未输入商品ID"),
    GOODS_SEARCH_ERROR(201, "没有符合条件的商品"),
    GOODS_ADDNULL(308,"请填写文件地址！");


    int code;
    String msg;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    GoodsEnum() {
    }

    GoodsEnum(String msg) {
        this.msg = msg;
    }

    GoodsEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
