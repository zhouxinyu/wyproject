package com.kgc.cn.enums;

/**
 * @auther zhouxinyu
 * @data 2019/12/13
 */
public enum EmployeeEnum {
    EMP_ADDSUCCESS("员工表添加成功！"),

    EMP_ADDFAILTURE(201, "员工表添加失败！"),

    EMP_NOFILR(202, "请填写excel表格路径！"),

    EMP_ADDREPEAT(203, "重新入职者："),

    EMP_ADDRESTART(204, "添加员工中身份证号码重复，重复着姓名："),

    EMP_ADDRESTARTANDRESTART(205, "有重复有重新入职"),

    EMP_ADDNULL(206,"请输入文件地址！"),

    EMP_UPDPWDSUCCESS(207,"密码修改成功，请重新登录！"),

    EMP_UPDPWDFAIL(208,"密码修改失败！"),

    EMP_UPDPWDNULL(209,"请填写新密码！");





    int code;
    String msg;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    EmployeeEnum() {
    }

    EmployeeEnum(String msg) {
        this.msg = msg;
    }

    EmployeeEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
