package com.kgc.cn.commonService;

import com.kgc.cn.model.dto.Attendance;

import java.text.ParseException;

/**
 * Created by Ding on 2019/12/13.
 */
public interface AttendanceConmonService {
    //查询该员工有没有出勤
    Attendance queryAttendance(String empId);

    //增加一条出勤记录
    int insertAttendance(String empId) throws ParseException;

    //更新出勤时间
    int updateWorkTime(String empId) throws ParseException;
}
