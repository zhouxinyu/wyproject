package com.kgc.cn.model.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 出售记录表
 */
@Data
public class Goodsell implements Serializable {
    private static final long serialVersionUID = -6767413575502989741L;
    //商品id
    private String goodId;
    //出售数量
    private Integer sellNum;
    //出售总金额
    private Double sellMoney;

    private Integer numSum;
    private Double moneySum;

}