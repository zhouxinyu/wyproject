package com.kgc.cn.model.dto;

import lombok.Data;

@Data
public class Role {
    //角色id
    private Integer roleId;
    //角色名称
    private String roleName;
}