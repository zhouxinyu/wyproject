package com.kgc.cn.model.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class Department implements Serializable {
    private static final long serialVersionUID = -2179028608329095226L;
    //部门id
    private Integer departId;
    //部门名称
    private String department;
}