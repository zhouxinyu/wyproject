package com.kgc.cn.model.vo;

import com.kgc.cn.model.dto.GoodsType;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class GoodsVo implements Serializable {
    private static final long serialVersionUID = 2216038493911992105L;
    //商品id
    private String goodId;
    //商品名
    private String goodName;
    //单价
    private Integer goodPrice;
    //库存
    private Integer goodNum;
    //类型id
    private Integer typeId;
    //图片地址
    private String pictureSource;
    //商品简介
    private String goodContent;
    //是否下架：0：下架，1：未下架
    private Integer isDelete;
    // 商品类型名
    private String typeName;
    // 商品类型对象
    private GoodsType goodsType;

}