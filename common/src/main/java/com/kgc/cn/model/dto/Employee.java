package com.kgc.cn.model.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 员工表
 */
@Data
public class Employee implements Serializable {
    private static final long serialVersionUID = 5128413285634031104L;
    //员工id
    private String empId;
    //姓名
    private String empName;
    //性别：1：男，2：女
    private Integer empSex;
    //年龄
    private Integer empAge;
    //邮箱
    private String empEmail;
    //手机号
    private String empPhone;
    //密码
    private String empPassword;
    //地址
    private String empAddress;
    //身份证
    private String empIdentityCard;
    //入职时间
    private String empTimeStart;
    //工资
    private Double empSalary;
    //是否离职：0：在职，1：离职
    private Integer isDelete;
    //权限id
    private Integer roleId;
    //部门id
    private Integer departId;
    //部门名称
    private String departName;

    // 职位名
    private String roleName;
    // 性别信息
    private String empSexMsg;
    // 职工状态
    private String isDeleteMsg;
}